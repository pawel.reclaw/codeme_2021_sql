-- Wypisz trenerów z ich kwalifikacjami do wykonywania bloków tematycznych
select * from `kursy_codeme`.`trener` t 
	inner join `kursy_codeme`.`trener_posiada_kwalifikacje` kw on t.id = kw.trener_id
    inner join `kursy_codeme`.`blok_tematyczny_kursu` bt on bt.id = kw.blok_tematyczny_kursu_id;

-- listowanie kursantów
-- a. wszystkich
select * from `kursy_codeme`.`kursant`;

-- b. kursantów zapisanych na konkretny kurs
select * from 
	`kursy_codeme`.`kursant` k 
		inner join `kursy_codeme`.`kursant_uczestniczy_w_kursie` ucz ON k.id = ucz.kursant_id
		inner join `kursy_codeme`.`kurs` kurs on ucz.kurs_id = kurs.id;
        
-- c. zainteresowanych kursem
select * from 
	`kursy_codeme`.`kursant` k 
		inner join `kursy_codeme`.`zgloszenie` ucz ON k.id = ucz.kursant_id
		inner join `kursy_codeme`.`kurs` kurs on ucz.kurs_id = kurs.id;
        
select count(*) as 'Liczba zainteresowanych kursem', kurs.id, kurs.nazwa from 
	`kursy_codeme`.`kursant` k 
		inner join `kursy_codeme`.`zgloszenie` ucz ON k.id = ucz.kursant_id
		inner join `kursy_codeme`.`kurs` kurs on ucz.kurs_id = kurs.id
	group by kurs.id;
	
-- dodawanie:
-- a. zainteresowanego (+ kursanta)
START transaction;
INSERT INTO `kursy_codeme`.`kursant` VALUES (null, 'Jan', 'Kowalski', 'jan.kowalski@polska.pl');
INSERT INTO `kursy_codeme`.`zgloszenie`VALUES (null, now(), last_insert_id(), ID_KURSU);
COMMIT;

-- b. kursanta (do kursu)
-- ID_KURSANTA
INSERT INTO `kursy_codeme`.`kursant_uczestniczy_w_kursie`VALUES (ID_KURSANTA, ID_KURSU, 0);

-- c. trenera 
INSERT INTO `kursy_codeme`.`trener` VALUES (null, 'Jan', 'Kowalski', now());

-- d. dodawanie kwalifikacji dla trenera
INSERT INTO `kursy_codeme`.`trener_posiada_kwalifikacje` VALUES (TRENER_ID, BLOK_TEMATYCZNY_ID);

-- dodać opinię kursanta do kursu/trenera
-- opinia wychodzi od konkretnego kursanta, do konkretnego trenera i bloku.
INSERT INTO `kursy_codeme`.`ocena` VALUES (null, 3, 'Źle prowadzi zajęcia.', now(), KURSANT_ID, TRENER_ID, TRENER_PROWADZI_BLOK_TEMATYCZNY_ID);

-- Obliczanie średniej oceny trenera z bloku
select t.imie, t.nazwisko, avg(o.ocena) from `kursy_codeme`.`trener` t
	inner join `kursy_codeme`.`ocena` o on t.id = o.trener_id
    inner join `kursy_codeme`.`trener_prowadzi_blok_tematyczny` pbt on o.trener_prowadzi_blok_tematyczny_id = pbt.id
    group by pbt.id;
    
-- dodanie:
-- a. rodzaju kursu
insert into `kursy_codeme`.`rodzaj_kursu` values(1, `sql`, 30, 2, 15, 12);
insert into `kursy_codeme`.`rodzaj_kursu` values(2, `c#`, 30, 2, 15, 12);
insert into `kursy_codeme`.`rodzaj_kursu` values(3, `java`, 30, 2, 15, 12);
insert into `kursy_codeme`.`rodzaj_kursu` values(4, `c++`, 30, 2, 15, 12);

-- b. dodanie kursu
insert into `kursy_codeme`.`kurs` values (null, null, null, `dzienny/zdalny`, 1, 'SQL, podstawy, kurs dzienny, zdalny', 1500.0);
insert into `kursy_codeme`.`kurs` values (null, null, null, `weekendowy/zdalny`, 1, 'SQL, podstawy, kurs weekendowy, zdalny', 1500.0);
insert into `kursy_codeme`.`kurs` values (null, null, null, `dzienny/stacjonarny`, 1, 'SQL, podstawy, kurs dzienny, stacjonarny', 1500.0);

-- Obliczanie zysku z kursów
-- KURS_ID
-- Obliczanie zysku
select sum(ucz.procent_znizki/100 * kurs.cena_kursu) as 'Przychód z kursu' from `kursy_codeme`.`kursant_uczestniczy_w_kursie` ucz 
		inner join `kursy_codeme`.`kurs` kurs on ucz.kurs_id = kurs.id
        where kurs.id = KURS_ID;

-- Obliczenie strat
select sum(tpb.ilosc_godzin*u.stawka) as 'Koszt trenerów' from `kursy_codeme`.`trener_prowadzi_blok_tematyczny` tpb 
        inner join `kursy_codeme`.`umowa` u on tpb.umowa_id = u.id
        where tpb.kurs_id = KURS_ID;

-- TRENER_ID
-- Wylistowanie wszystkich dni zajęć trenera:
select t.imie, t.nazwisko, date(z.czas_rozpoczecia) from `kursy_codeme`.`trener_prowadzi_blok_tematyczny` tpb 
		inner join `kursy_codeme`.`trener` t on tpb.trener_id = t.id
        inner join `kursy_codeme`.`zajecia` z on tpb.id = z.trener_prowadzi_blok_tematyczny_id
        where t.id = TRENER_ID;
