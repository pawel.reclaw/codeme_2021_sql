select
	srvc_name,
	min_participants,
	per_person_price
from dbo.additional_service;

-- 1. find the rows where the service name is 'Catering - Lunch'

-- 2. find the rows where the service name is not 'Catering - Lunch'

-- 3. find the rows where the service name is 'Gift Basket Delivery - Small', 'Gift Basket Delivery - Medium', or 'Gift Basket Delivery - Large'

-- 4. find the rows where the service name is not 'Gift Basket Delivery - Small', 'Gift Basket Delivery - Medium', or 'Gift Basket Delivery - Large'

-- 5. find the rows where the service name starts with 'Gift Basket Delivery'

-- 6. find the rows where the service name contains 'Party'

-- 7. find the rows where the service name does not start with 'Gift Basket Delivery'

-- 8. find the rows where the price per person is between $75 and $125

-- 9. find the rows where the price per person is less than $75

-- 10. find the rows where the price per person is $125 or more

-- 11. when are NULL values returned?

-- 12.find the rows for services that require no more than 6 participants and costs no more than $25 per person

-- 13. filter again to just show the catering services that meet the last criteria

-- 14. find the rows for either catering services, gift baskets, or the Two Trees Tasting Party

-- 15. find the rows for services that cost less than $30 that are either catering or gift baskets