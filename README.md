# codeme_2021_sql

## Description
This repository contains all fieles collected during SQL Training Academy of CODE:ME Foundation. State of this repository always shows current progress of the group. Classess started on the 17th of May. So far, two classes have been conducted with material on creating databases and performing basic queries. 

## Contents and files
### Day 1 (17 of May 2021)
On the first classes we've created files:
- THESE FILES ARE INSIDE DIRECTORY `class_1\materialy_z_zajec\`
- `class_1_basics.sql` - file containing few notes and examples of create queries
- `class_1_client.sql` - file containing examples of queries along with comments explaining their construction

### Day 2 (20 of May 2021)
Second classes following files have been presented:
- THESE FILES ARE INSIDE DIRECTORY `class_1\zad_dom\`
- `class_2_homework_1.sql` - solution to homework task 2 from first classes.
- `class_2_homework_2.sql` - solution to homework task 2 from first classes.
- THESE FILES ARE INSIDE DIRECTORY `class_2\materialy_z_zajec`
- `class_2_dane_klient.sql` - example data used along with presentation.
- `class_2_rozwiazania_klient.sql` - example queries used along with presentation. These were queries on which queries were explained. File contains comments and explanations on usage.
- THESE FILES ARE INSIDE DIRECTORY `class_2\zad_dom\polecenie_1`
- `class_2_homework_dane.sql` - contents of this file are queries with data used on classes. Data was used for on-class exercises.
- `class_2_homework_tabele.sql` - contents of this file are database creational queries used on classes. Data was used for on-class exercises.

### Day 3 (24 of May 2021)
- THESE FILES ARE INSIDE DIRECTORY `class_2\zad_dom\polecenie_2`
- `class_3_homework_1` - contain description of queries with **higher dificulty** and this **should be done second** if You encounter problems.
- `class_3_homework_2...` - contain data (`dane`) and homework description of queries(`polecenia`) that should be created by students. These queries are easier.
- THESE FILES ARE INSIDE DIRECTORY `class_3\materialy_z_zajec`
- We've mostly worked on `\1_baza_kontrola`. 

## Day 4 (27 of May 2021)
For the fourth classes that will take place on the 27th of May 2021 files named with prefix `class_4` were presented.
We have homework described:
- do exercises from `class_3\zad_dom` - there is a special: `readme.zad_dom.txt` You can use where there is more description.
- data for class_4 will be presented on the 25th of May by the end of the day along with solutions to the homework.
- PLEASE REMEMBER TO NOTE YOURSELF ALL TASKS THAT YOU HAVE QUESTIONS TO.

## Day 5 (31 of May 2021)
Do zrealizowania mamy: 
- `class_3/zad_dom/zad_dom_1` - mamy 4 foldery z zadaniami do zrealizowania
- `class_3/zad_dom/zad_dom_2` - mamy plik `class_3_polecenia_1_countries.sql`
- Repozytorium zawiera jedną bazę danych z poleceniami na SELECT, UPDATE, INSERT, DELETE

## Aktualne zadania domowe
Do zrealizowania mamy: 
- Zaległe zadania domowe. Zapytania countries oraz 4 foldery z zapytaniami SQL.
- Stworzyć projekty baz danych. Wewnątrz folderu `class_5\materiały` znajdują się bazy danych numerowane:
		- 0 (zrealizowaliśmy w 100% na zajęciach).
		- 1
		- 2
		- 4
		- 5
  Należy zrealizować (stworzyć projekt [plik .mwb] bazy oraz wygenerować z niego bazy danych w schema, a następnie stworzyć kontrolne zapytania INSERT oraz SELECT do baz danych). Możesz opcjonalnie wybrać realizację zadania 4 lub 5. Wszystkie wcześniejsze są obowiązkowe.
  
  
### We've currently finished all tasks with queries but homework related to them will still appear.

## Caveat
Please contact me whenever You encounter problems.

## Author
Paweł Recław

