## Projektowanie baz danych
# 1.
Stwórz projekt, który ma być pomocny do organizowania wyjazdów i robienia sobie 'checklisty' (np. wybieram się na piknij i zrobiłem sobie listę rzeczy które muszę ze sobą zabrać):
- koc
- piwko
- koszyk owoców
- żonę

Aplikacja ma pozwalać tworzenie "List do get" - lista rzeczy które muszę wziąć/zabrać. 

Chcemy przechowywać checklisty wielu użytkowników.
Chciałbym mieć możliwość odpytania o listy które wciąż posiadają nieukończone zadania.
Chciałbym mieć możliwość odpytania o wszystkie nieukończone zadania ze wszystkich list.

Sugestia - nigdy nie nazywać tabel 'user'. W niektórych językach programowania jest to zabronione.
Tabele:
- 'system_user'
- 'checklist'
- 'todo_task'