INSERT INTO `mydb`.`system_user`
(`id`,
`first_name`,
`last_name`)
VALUES
(NULL, 'Pawel', 'Gawel'),
(NULL, 'Jacek', 'Kowalski'),
(NULL, 'Małgosia', 'Nowak');

INSERT INTO `mydb`.`checklist`
(`id`,
`name`,
`system_user_id`)
VALUES
(null, 'Piknik', 1),
(null, 'Rzeczy do pracy', 1);

INSERT INTO `mydb`.`todo_task`
(`id`,
`name`,
`checklist_id`,
`finished`)
VALUES
(null, 'Kocyk', 1, null),
(null, 'Piwko', 1, null),
(null, 'Winko', 1, null),
(null, 'Kosz owoców', 1, null),
(null, 'Żona', 1, now()),
(null, 'Śniadanie', 2, null),
(null, 'Komputer', 2, now());

-- znajdz wszystkie nieukonczone zadania uzytkownika 'Pawel Gawel' 
select * 
	from `todo_task` t 
		join `checklist` c 
		join `system_user` u
        on t.checklist_id = c.id 
			and u.id = c.system_user_id
    where 
		t.`finished` is null
        and u.first_name = 'Pawel'
        and u.last_name = 'Gawel';


-- znajdz wszystkie nieukonczone zadania uzytkownika o id 1 
select * from `todo_task` t join `checklist` c on t.checklist_id = c.id where c.id = 1 and t.`finished` is null;



