CREATE DATABASE IF NOT EXISTS `codeme_homework`;
USE `codeme_homework`;
DROP TABLE IF EXISTS `table_homework_1_companies`;
CREATE TABLE IF NOT EXISTS `table_homework_1_companies` (
    #  nazwa_kolumny        typ_danych		dodatkowe_opcje,
    `id_user` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `first_name` VARCHAR(60) NOT null,
    `last_name` VARCHAR(60) NOT null,
    `established_date` DATETIME NOT null DEFAULT NOW() ,
    -- `established_date` DATE NOT null DEFAULT DATE(NOW()),
    -- - address                    [opcjonalny parametr]
    `address` TEXT DEFAULT Null,
    `company_name` VARCHAR(50) NOT null,
    `tax_id_number` VARCHAR(13) DEFAULT Null 
);
 
TRUNCATE `table_homework_1_companies`;
#3. Wstaw kilka rekordów na następujący sposób:
#- kilka w domyślnym formacie [podając wszystkie parametry]
 
INSERT INTO `table_homework_1_companies` VALUES (1, 'Paweł', 'Recław', '2021-05-19', 'Słoneczna 10, 00-100 Warszawa', 'Codeme Sp. z o.o.','100-999-01-23');
INSERT INTO `table_homework_1_companies` VALUES (2, 'Ixinski', 'Igrek', '2021-05-19', 'Dolna 20, 00-999 Warszawa', 'IXI Sp. z o.o.','100-200-01-00');
INSERT INTO `table_homework_1_companies` VALUES (3, 'Jacek', 'Placek', '2021-05-19', 'Wiosenna 15, 85-100 Bydgoszcz', 'Gryf Sp. z o.o','554-005-10-30');
INSERT INTO `table_homework_1_companies` (`first_name`, `last_name`, `company_name`) VALUES ('Jan', 'Kowalski', 'Firma');
 
#- kilka bez podania id [jeśli wiesz jak użyć auto_increment]
 
INSERT INTO `table_homework_1_companies` VALUES (null, 'Maria', 'Curie-Skłodowska', '2010-04-12', 'Letnia 1, 66-400 Gorzów Wielkopolski', 'Stilon Sp. z o.o','599-001-12-34');
INSERT INTO `table_homework_1_companies` VALUES (default, 'Piotr', 'Curie', '2010-04-12', 'Jesienna 6, 66-430 Zielona Góra', 'Falubaz Sp. z o.o','660-001-12-34');
 
 
#- wstaw kilka rekordów bez podawania NIP i/lub daty założenia/adresu.
 
INSERT INTO `table_homework_1_companies` VALUES (default, 'Jan', 'Testowy', default, default, 'Silwana Sp. z o.o',default);
INSERT INTO `table_homework_1_companies` (first_name, last_name, company_name) VALUES 
('Jacek',     'Szymański', 'Philips Lighting'),
('Igor', 'Stefański', 'LG Displays'),
('Piotr', 'Olszewski', 'Smasung Slovakia');


-- id_user - NOT NULL jest ustawione domyślnie jeśli używamy PRIMARY KEY.
-- DEFAULT CURRENT_TIMESTAMP - domyślne ustawienie daty i czasu dla kolumny. Timestamp zawiera zarówno datę i godzinę.
-- DATE jest typem który jest precyzyjny do pełnego dnia. DATETIME zawiera również informację o czasie.
