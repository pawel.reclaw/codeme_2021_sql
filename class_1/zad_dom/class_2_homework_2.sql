#1. Wykorzystaj (dodaj komende use) baze danych codeme_homework
USE `codeme_homework`;

#2. Stwórz tabelę table_homework_2_students z polami:
#- id_user                     (primary key, int) [jeśli wiesz jak, użyj Auto_increment]
#- pesel                          [ koniecznie chcemy zapewnic zeby uzytkownik wstawil 11 znaków, nie może być puste: https://stackoverflow.com/questions/63727300/mysql-require-a-minimum-length]
#
#- grades_average             [domyślnie 0.0, nie może być pusta, jest to liczba która nie jest całkowita]

DROP TABLE IF EXISTS `table_homework_2_students`;

CREATE TABLE IF NOT EXISTS `table_homework_2_students` (
	`id_user` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `pesel` VARCHAR(11) NOT NULL ,
    -- `pesel` CHAR(11) NOT NULL ,
    `grades_average` FLOAT	NOT NULL DEFAULT '0.0'
);
 ALTER TABLE `table_homework_2_students` ADD CONSTRAINT NAZWA CHECK (CHAR_LENGTH(`pesel`) >= 11);

#3. Wstaw kilka rekordów na następujący sposób:
#- kilka w domyślnym formacie [podając wszystkie parametry]
#- kilka bez podania id [jeśli wiesz jak użyć auto_increment
#- wstaw kilka rekordów bez podawania NIP i/lub daty założenia/adresu.

INSERT INTO `table_homework_2_students` VALUES 
(default, '79090112345', '5.0'),
(default, '85020112555', '4.7'),
(default, '95020112555', '4.5');

INSERT INTO `table_homework_2_students` (`pesel`) VALUES
('01020312345'),
('99123112345'),
('98111112345');