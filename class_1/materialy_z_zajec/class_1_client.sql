-- 1. Dodaj wiersz który tworzy bazę danych `codeme_client_db`, użyj tej bazy danych (use)
CREATE DATABASE IF NOT EXISTS `codeme_client_db`;

USE `codeme_client_db`;

-- 2. Popraw budowę tabeli tak, aby kolumny były w gres'ach
-- 2b. Dodaj klauzule która warunkuje stworzenie tabeli/bazy tym, czy już nie zostały stworzone/nie istnieją.
DROP TABLE `klient`;
CREATE TABLE IF NOT EXISTS `klient` (
	`klient_id` INT AUTO_INCREMENT PRIMARY KEY,
	`imie`                CHAR(100) CHECK (CHAR_LENGTH(`imie`) >= 3),
	`nazwisko`            CHAR(100),
	`rok_urodzenia`       DATE,
	`plec`                CHAR(1),
	`data_zalozenia`      DATE
);

INSERT INTO `klient` (`imie`, `nazwisko`, `rok_urodzenia`, `plec`, `data_zalozenia`) VALUES 
('Jacek',     'Szymański', '1970-01-01', 'M', '2014-01-01'),
('Jan',        'Kowalski',  '1980-01-01', 'M', '2014-01-10'),
('Franciszek', 'Kowalczyk', '1981-01-01', 'M', '2014-01-10'),
('Joanna',     'Nowak',     '2000-02-01', 'K', '2014-01-10'),
('Tadeusz',    'Nowak',     '1972-01-01', 'M', '2014-01-10'),
('Jacek',     'Szymański', '1950-01-01', 'M', '2014-01-01'),
('Paweł',     'Gaweł', '1990-05-01', 'M', '2000-01-01');

-- 3. Dodaj dwa nowe rekordy do bazy danych stosując notację pełną [oddzielny wiersz = nowy rekord] i jeden rekord dopisując krotkę w nawiasach.
INSERT INTO `klient` VALUES (NULL, 'Pawełek', 'Gawełek', '1991-01-01', 'M', '2001-01-01')

-- Notacja Workbench:
-- INSERT INTO `codeme_client_db`.`klient` (`klient_id`, `imie`, `nazwisko`, `rok_urodzenia`, `plec`) VALUES ('9', 'hj21b', 'kjb', 'hjb', 'hjb');
