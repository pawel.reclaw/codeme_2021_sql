# Wypisz wszystkie ksiazki.
SELECT * FROM ksiegarnia.books;

# Wypisz wszystkie ksiazki posortowane wedlug tytulu a-z.
SELECT * FROM ksiegarnia.books ORDER BY `title` ASC;

# Wypisz wszystkie ksiazki posortowane wedlug tytulu z-a.
SELECT * FROM ksiegarnia.books ORDER BY `title` DESC;

# Wypisz wszystkie ksiazki posortowane wedlug kategorii i tytulu a-z.
SELECT * FROM ksiegarnia.books ORDER BY `category`, `title`;

# Wypisz wszystkie ksiazki opublikowane po 2012-12-31.
SELECT * FROM ksiegarnia.books where `published` is not null and `published` > '2012-12-31';

# Wypisz wszystkie tytuly ksiazek Marcina Lisa.
SELECT `title` FROM ksiegarnia.books where `author`='Marcin Lis';

# Wypisz wszystkie ksiazki napisane przez autora innego niz Marcin Lis w kategorii bazy danych.
SELECT * FROM ksiegarnia.books where `author`<>'Marcin Lis' and `category`='Bazy danych';

# Wypisz wszystkie kategorie, w ktorych jest wiecej niz jedna ksiazka
SELECT `category`,COUNT(*) FROM ksiegarnia.books group by `category` having count(*) > 1;

# Wypisz tytuly i autorow ksiazek w cenie od 50 do 100 zlotych.
SELECT `title`,`author` FROM ksiegarnia.books where `price` between 50 and 100;

# Wypisz wszystkie ksiazki o okreslonej dacie publikacji (nie NULL)
SELECT * FROM ksiegarnia.books where `published` is not null;

# Wypisz dane o 2, 3 i 4-tej najdrozszej ksiazce.
-- sortowanie od najdroższych:
-- k1   140
-- k2   90
-- k3   76
-- k4   40
-- k5   32
-- k6   30
-- k7   11
-- limit mówi ile rekrdów nas interesuje
-- offset - mówi od którego rekordu zacząć
SELECT * FROM ksiegarnia.books order by `price` desc limit 3 offset 1;

select * from books E1 where (2 - 1) = (select count(distinct(price)) 
                from books E2 
                where E2.price > E1.price ) 
 UNION                
  select * from books E1 where (3 - 1) = (select count(distinct(price)) 
                from books E2 
                where E2.price > E1.price ) 
UNION
select * from books E1 where (4 - 1) = (select count(distinct(price)) 
                from books E2 
                where E2.price > E1.price );
                
SELECT * FROM books ORDER BY price DESC LIMIT 1,3;

SELECT * FROM ksiegarnia.books ORDER BY price DESC LIMIT 5 OFFSET 1;

SELECT distinct `price` FROM `books` ORDER BY `price` DESC LIMIT 1 OFFSET 3 ; -- zwraca nam 4 cenę (69.00)
SELECT distinct `price` FROM `books` ORDER BY `price` DESC LIMIT 1 OFFSET 1; -- zwraca nam 2 cenę (149.00)


SELECT * FROM `books` WHERE `price` BETWEEN (69.00) and 149.00;
SELECT * FROM `books` WHERE `price` BETWEEN (SELECT distinct `price` FROM `books` ORDER BY `price` DESC LIMIT 1 OFFSET 3) and (149.00);
SELECT * FROM `books` WHERE `price` BETWEEN (SELECT distinct `price` FROM `books` ORDER BY `price` DESC LIMIT 1 OFFSET 3) and (SELECT distinct `price` FROM `books` ORDER BY `price` DESC LIMIT 1 OFFSET 1);

# Wypisz wszystkie tytuly ksiazek zaczynajace sie od tekstu "Angular".
SELECT `title` FROM ksiegarnia.books where `title` LIKE 'Angular%';

# Wypisz wszystkie tytuly ksiazek o programowaniu.
SELECT `title`, `category` FROM ksiegarnia.books where `title` LIKE '%program%' or `category` like '%program%';

# Wypisz liczbę ksiazek kazdego autora posortowana wedlug liczby malejaco.
SELECT `author`, count(`author`) as 'ilosc' FROM ksiegarnia.books group by `author` order by count(`author`) desc;

# Wypisz laczna liczbe stron wszystkich ksiazek o bazach danych.
SELECT SUM(`page_count`) FROM `books` group by `category`;
SELECT SUM(`page_count`) FROM `books` where `category`='Bazy danych';

# Wypisz dane o najdrozszej ksiazce.
SELECT * FROM `books` order by `price` desc limit 1;

-- zapytanie o książkę z najwyższą ceną: (SELECT MAX(price) FROM books)
-- drugie zapytanie wykorzystuje wynik 1 zapytania
SELECT * FROM books WHERE price = (SELECT MAX(price) FROM books);

-- to rozwiązanie dopisuje nam dodatkowo kolumnę
-- SELECT *, max(price) FROM books; 
