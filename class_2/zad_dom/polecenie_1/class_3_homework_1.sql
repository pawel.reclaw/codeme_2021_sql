-- Wyświetlić (kolumny id, login i hasło) wszystkie rekordy z tabeli admin, bibliotekarz i czytelnik.
SELECT `id_admin` as id, `login`, `haslo` FROM `admin`
UNION
SELECT `id_bibliotekarz` as id, `login`, `haslo` FROM `bibliotekarz`
UNION 
SELECT `id_czytelnik` as id, `login`, `haslo` FROM `czytelnik`;

#2 Liczba książek wydanych na autora
-- Napisać zapytanie, które wyświetli liczbę wydanych książek poszczególnego 
-- autora, np. Michael Feathers, 1; Robert C. Martin 2; itd.

-- count + group by - podaje liczebności grup
SELECT `autor` as 'Autor', COUNT(`autor`) as 'Ilość książek' FROM `ksiazka` GROUP BY `autor`;
SELECT COUNT(*) FROM `ksiazka` GROUP BY `autor`;

-- count - zlicz rekordy (wszystkie w tabeli)
SELECT COUNT(*) FROM `ksiazka`;

#3 Rozwinięcie zadania #2
-- Napisać zapytanie, które wyświetli liczbę wydanych ksiązek poszczególnego 
-- autora, który wydał co najmniej 2 książki

-- HAVING jest klauzulą WHERE dla wyników które otrzymaliśmy z GROUP BY
SELECT `autor`,COUNT(*) from `ksiazka` group by `autor` having count(*) >=2;
SELECT autor, COUNT(autor) as qty FROM ksiazka K GROUP BY K.autor HAVING COUNT(autor)>1  ;


#4 Różnica pomiędzy datami
-- Napisać zapytanie, które wyświetli liczbę dni do odbioru od zamówienia
-- datediff - różnica dat
SELECT DATEDIFF(`data_odbioru`, `data_zamowienia`) from `zamowienie`;

#5 Różnica pomiędzy datami - cd
-- Napisać zapytanie, które wyświetli liczbę dni od odbioru do teraz... jeżeli pozycja (wartość kolumny odbioru `data_odbioru`) nie zostałą zwrócona.
SELECT DATEDIFF(now(), `data_zamowienia`) from `zamowienie` where `data_odbioru` is null;

-- Napisać zapytanie, które wyświetli liczbę dni od odbioru do teraz... jeżeli pozycja (wartość kolumny odbioru `data_zwrotu`) nie zostałą zwrócona.
SELECT DATEDIFF(now(), `data_odbioru`) from `zamowienie` where `data_zwrotu` is null and `data_odbioru` is not null;

#6 Wyświeltić wszystkie kategorie po przecinku
#7 Wyświetlić wszystkie kategorie po średnika
-- zlicz ile jest książek danej kategorii i wyswietl w formacie nazwa kategorii - ilosc ksiazek tej kategorii
SELECT COUNT(cat.`id_kategoria`) as 'Ilosc ksiazek', cat.`nazwa` as 'Nazwa' 
	FROM `ksiazka` k, `kategoria` cat 
		WHERE k.id_kategoria = cat.id_kategoria 
			GROUP BY cat.`id_kategoria`;
            
-- inne rozwiązanie ze zliczeniem po użyciu LEFT JOIN.
-- Left Join sprawia, że rekordy lewej tabeli wyświetlą się zawsze, a prawej tabeli tylko, jeśli będą odpowiadające rekordy po lewej.
-- jeśli rekordy z lewej tabeli nie posiadają żadnych powiązań, to wszystkie kolumny tabeli prawej zostaną wypełnione wartością `null`
SELECT Kat.nazwa, COUNT(K.id_kategoria) as qty FROM kategoria Kat LEFT JOIN ksiazka K on Kat.id_kategoria = K.id_kategoria GROUP BY Kat.nazwa;


-- wyswietl jakie ksiazki wypozyczył czytelnik który ma w imieniu leżące koło siebie litery pi albo na końcu imienia litery yk
-- wyswietl ile książek mają czytelnicy z imieniem Piotr i nazwiskiem klimek
-- wyswietl jakie ksiazki sa nie wypozyczone, posortuj je alfabetycznie po tytule ksiazki a nastepnie po opisie
-- wyswietl ile jest wypozyczonych ksiazek
-- wyswietl wypozyczone kategorie ksiazek przy czym wyswietl te ktorych jest wiecej niz 3 posortuj je malejąco
-- wyswietl ludzi którzy wypozyczyli ksiazki z kategorii programowanie, Poradniki, Webmasterstwo oraz maja wypozyczone liczbą różną od 1 ksiązek
-- wyswietl ludzi którzy wypozyczyli ksiazki z kategorii która w nazwie posiada ciąg 'GRAM' oraz nie oddali ksiazek do wypozyczalni oraz odebrali je po 2014-08-03
-- wyswietl unikalnych czytelników którzy zamówili ksiązkę pomiędzy 2014-08-01 oraz katergoria ksiązki to Programowanie a teraźniejszą datą ogranicz ich liczbe do 4

select * from zamowienie;