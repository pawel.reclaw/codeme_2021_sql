USE biblioteka; 

-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie kategorii do systemu 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.kategoria VALUES(1, 'Biznes'); 
INSERT INTO biblioteka.kategoria VALUES(2, 'Poradniki'); 
INSERT INTO biblioteka.kategoria VALUES(3, 'Programowanie'); 
INSERT INTO biblioteka.kategoria VALUES(4, 'Programowanie mobilne'); 
INSERT INTO biblioteka.kategoria VALUES(5, 'Webmasterstwo'); 
INSERT INTO biblioteka.kategoria VALUES(6, 'Systemy operacyjne'); 


-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie bibliotekarza do systemu 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.bibliotekarz VALUES(null, 'bibliotekarz', 'bibliotekarz'); 


-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie administratora do systemu 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.admin VALUES(null, 'admin', 'admin'); 


-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie czytelników do systemu 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.czytelnik VALUES(1, 'czytelnik_1', 'hasło_czytelnika', 'Piotr', 'Klimek', 'ul. Przykład 4/ 12', 'Lublin', 'Lubelskie', null, '20-998', 'przyklad_1@sdacademy.pl'); 

INSERT INTO biblioteka.czytelnik VALUES(2, 'czytelnik_2', 'hasło_czytelnika', 'Patryk', 'Klimek', 'ul. Przykład 10/300', 'Lublin', 'Lubelskie', null, '20-999', 'przyklad_2@sdacademy.pl'); 


-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie książek do systemu 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.ksiazka VALUE(1, 3, 9788324631773, 'PHP i MySQL. Tworzenie stron WWW. Vademecum profesjonalisty. Wydanie czwarte', 'Luke Welling, Laura Thomson', 856, 'Helion', 2009, 'Czwarte wydanie bestsellerowego podręcznika dla webmasterów wykorzystujących w swojej pracy funkcjonalność języka PHP i bazy danych MySQL.'); 

INSERT INTO biblioteka.ksiazka VALUE(2, 3, 9788324685301, 'Język C++. Kompendium wiedzy', 'Bjarne Stroustrup', 1296, 'Helion', 2014, null); 

INSERT INTO biblioteka.ksiazka VALUE(3, 3, 9788324675340, 'Mistrz czystego kodu. Kodeks postępowania profesjonalnych programistów', 'Robert C. Martin', 216, 'Helion', 2013, null); 

INSERT INTO biblioteka.ksiazka VALUE('4', '6', '9788324690138', 'Kali Linux. Testy penetracyjne', 'Joseph Muniz, Aamir Lakhani', '336', 'Helion', '2014', null); 

INSERT INTO biblioteka.ksiazka VALUE(5, 3, 9788324621880, 'Czysty kod. Podręcznik dobrego programisty', 'Robert C. Martin', 424, 'Helion', 2010, null); 

INSERT INTO biblioteka.ksiazka VALUE(6, 3, 9788324632374, 'Pragmatyczny programista. Od czeladnika do mistrza', 'Andrew Hunt, David Thomas', 332, 'Helion', 2011, null); 

INSERT INTO biblioteka.ksiazka VALUE(7, 3, 9788324683178, 'Praca z zastanym kodem. Najlepsze techniki', 'Michael Feathers', 440, 'Helion', 2014, null); 

INSERT INTO biblioteka.ksiazka VALUE(8, 5, 9788324685042, 'Tajemnice JavaScriptu. Podręcznik ninja', 'John Resig, Bear Bibeault', 432, 'Helion', 2014, null); 

INSERT INTO biblioteka.ksiazka VALUE(9, 3, 9788324689361, 'Java EE 6. Tworzenie aplikacji w NetBeans 7', 'David R. Heffelfinger', 352, 'Helion', 2014, null); 

INSERT INTO biblioteka.ksiazka VALUE(10, 5, 9788324666676, 'Projektowanie stron internetowych. Przewodnik dla początkujących webmasterów po HTML5, CSS3 i grafice. Wydanie IV', 'Jennifer Niederst Robbins', 600, 'Helion', 2014, null); 


-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie zamówień dla czytelnik_1 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.zamowienie SET id_czytelnik=1, id_ksiazka=1, data_zamowienia='2014-08-01 10:12:02'; 

INSERT INTO biblioteka.zamowienie SET id_czytelnik=1, id_ksiazka=2, data_zamowienia='2014-08-01 10:12:02', data_odbioru='2014-08-03 12:10:10'; 

INSERT INTO biblioteka.zamowienie SET id_czytelnik=1, id_ksiazka=5, data_zamowienia='2014-08-01 10:13:02', data_odbioru='2014-08-03 12:11:10', data_zwrotu='2014-08-15 12:00:00'; 


-- ----------------------------------------------------- 
-- Skrypt odpowiedzialny za dodanie dwóch zamówień dla czytelnik_2 
-- ----------------------------------------------------- 
INSERT INTO biblioteka.zamowienie SET id_czytelnik=2, id_ksiazka=3, data_zamowienia='2014-08-02 12:00:02'; 

INSERT INTO biblioteka.zamowienie SET id_czytelnik=2, id_ksiazka=4, data_zamowienia='2014-08-03 09:12:02', data_odbioru='2014-08-05 15:20:00'; 