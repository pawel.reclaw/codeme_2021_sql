use `code20_template`;

-- #####################################################
-- ########    Przykłady z prezentacji            ######
-- #####################################################

SELECT imie, nazwisko FROM klient;

SELECT * FROM klient;

-- Nazwisko rozpoczyna się od Kowa ale może zakończyć się (% - dowolny) dowolnymi znakami
SELECT * FROM klient WHERE nazwisko LIKE 'Kowa%';

-- Case not sensitive
-- Case sensitive - bierze pod uwagę wielkość liter
SELECT * FROM klient WHERE nazwisko='Kowalski';

-- Znajdz wszystkie rekordy które nie mają w kolumnie `nazwisko` 'Kowalski'
-- <> - negative - nie jest = (!=) (not)
SELECT * FROM klient WHERE nazwisko<>'Kowalski';

-- Operator logiczny LUB
SELECT * FROM klient WHERE nazwisko='Kowalski' OR nazwisko='Szymański';

-- Alias/zmiana nazwy dla tabel
SELECT * FROM `ksiazka`, `recenzja` WHERE `ksiazka`.ksiazka_id=`recenzja`.ksiazka_id;
SELECT * FROM `ksiazka` k, `recenzja` r WHERE k.ksiazka_id = r.ksiazka_id;

-- Przykład zastosowania aliasu dla kolumn. Ciężko znalezc inne rozwiazanie dla takiego problemu.
SELECT * FROM `osoba` rodzic, `osoba` dziecko WHERE rodzic.osoba_id = dziecko.id_rodzic;

-- Połączenie wartości z dwóch tabel. Pewna uwaga - ilość kolumn w obu SELECT'ach musi być jednakowa. 
SELECT imie,nazwisko FROM code20_template.klient
UNION
SELECT imie,nazwisko FROM code20_template.klient_archiwalny;

-- Ilość kolumn musi być jednakowa. Typ/wartości nie mają znaczenia. Poniższe zapytanie również zadziała.
SELECT imie,nazwisko FROM code20_template.klient
UNION
SELECT imie,klient_id FROM code20_template.klient_archiwalny;

-- Rekordy które posiadają unikalne wartości pewnej kolumny
SELECT DISTINCT autor_imie FROM ksiazka;

SELECT klient_id as 'Identyfikator', imie as 'Imie klienta', nazwisko as 'Nazwisko klienta', data_zalozenia as 'Data dołączenia' FROM klient;

-- SELECT imie,nazwisko FROM code20_template.klient EXCEPT SELECT imie,nazwisko FROM code20_template.klient_archiwalny;

-- #####################################################
-- ########    Zadania, czesc 1 				  ######
-- #####################################################

-- 1. zwracany jedna kolumne (tytul) z tabeli ksiazka.
SELECT `tytul` FROM `ksiazka`;

-- 2. zwracamy dwie kolumny (tytul, autor_imie), z tabeli ksiazka
SELECT `ksiazka`.`tytul`, `ksiazka`.`autor_imie` FROM `ksiazka`;

-- 3. zwracamy trzy kolumny (tytul, autor_imie, autor_nazwisko)  z tabeli ksiazka. Wypisz tylko wiersze w ktorych min_wiek byl wiekszy od 12
SELECT ksiazka.tytul, autor_imie, autor_nazwisko, min_wiek FROM ksiazka WHERE min_wiek>2 order by min_wiek;

-- 4. Wszystkie kolumny z tabeli ksiazka.

-- 5. Chcemy wyswietlic polecajacego i osobę która została zwerbowana 
-- Dokonujemy połączenia tabeli `klient` z tabelą `klient`, dlatego konieczne jest dodanie aliasu/zmiana nazwy tabeli, żeby móc operować na dwóch tabelach.
SELECT * FROM `klient` polecajacy, `klient` zwerbowany WHERE polecajacy.klient_id = zwerbowany.id_polecony_przez;

