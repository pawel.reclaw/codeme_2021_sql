Zadanie domowe z zajęć 3:
- folder zad_dom_1 - zawiera 4 foldery z zapytaniami do przećwiczenia
- folder zad_dom_2 - zawiera treści zadań z bazy "countries" z zajęć 3. Do zrealizowania sugeruję pierwszą część tych poleceń. Drugi plik z poleceniami póki co pozostawimy sobie na kolejne zadanie domowe. Do folderu przekopiowałem treści.

Rozwiązania do zadań podeślę pod koniec dnia (podobnie, okolice 23) 25.05.2021. Znajdą się w folderze 'rozwiązania' konkretnego zadania.