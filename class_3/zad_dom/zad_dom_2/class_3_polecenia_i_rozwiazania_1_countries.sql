use `codeme_homework`;

-- 1. Wyświetl strukturę tabeli employees.
describe `employees`;

-- 2. Wyświetl zawartość tabeli employees.
select * from `employees`;

-- 3. Wyświetl imiona, nazwiska oraz pensje pracowników.
select first_name, last_name, salary from `employees`;

-- 4. Wyświetl imiona, nazwiska pracowników. Nadaj aliasy „imie”, „nazwisko” odpowiednim kolumnom.
select first_name as 'imie', last_name as 'nazwisko' from `employees`;

-- 5. Popraw błędy w zapytaniach:
select * from departments;

-- select name, from employees;
select name from employees;

-- select hire_date as ‘data zatrudnienia’ from employees;
select hire_date as 'data_zatrudnienia' from employees;

-- select name nazwisko pracownika from employees;
select first_name as 'nazwisko_pracownika' from employees;

-- 6. Wyświetl imiona i nazwiska pracowników w jednej kolumnie (konkatenacja).
select concat(first_name, ' ', last_name) from employees;

-- 7. Wyświetl alfabetyczną listę pracowników.
select concat(first_name, ' ', last_name) as 'combined' from `employees` order by last_name;
select first_name as 'imie', last_name as 'nazwisko' from `employees` order by first_name, last_name;

-- 8. Wyświetl nazwiska pracowników w porządku malejącym.
select last_name as 'nazwisko' from `employees` order by last_name desc;

-- 9. Wyświetl nazwiska i pensje pracowników w porządku malejącym wg pensji.
select last_name as 'nazwisko' from `employees` order by last_name desc;

-- 10. Wyświetl imiona, nazwiska i pensje pracowników w porządku rosnącym wg pensji i malejącym wg nazwisk.
select last_name as 'nazwisko', salary from `employees` order by salary asc, 'last_name' desc;

-- 11. Wyświetl listę nazwisk. W wyniku nie mogą pojawić się duplikaty nazwisk.
select distinct last_name as 'nazwisko' from `employees` ;

-- 12. Wyświetl dane pracowników o nazwisku King.
select last_name as 'nazwisko' from `employees` where last_name like 'King%';

-- 13. Wyświetl nazwiska pracowników, którzy zarabiają poniżej 5000.
select last_name as 'nazwisko', salary from `employees` where salary < 5000;

-- 14. Wyświetl imiona i nazwiska pracowników, których pensja znajduje się w przedziale [2000, 7 000].
select first_name as 'imie', last_name as 'nazwisko', salary from `employees` where salary  between 2000 and 7000;

-- 15. Wyświetl bez duplikatów identyfikatory stanowisk z tabeli employees.
select distinct job_id from `employees`;

-- 16. Wyświetl imię, nazwisko oraz datę zatrudnienia wszystkich pracowników, których pensja nie znajduje się w przedziale [5000, 10 000]. Wyniki posortuj rosnąco wg pensji.
select first_name as 'imie', last_name as 'nazwisko', salary, hire_date from `employees` where salary  between 5000 and 10000 order by salary asc;

-- 17. Wyświetl dane osób o identyfikatorach 100, 102, 105 i 107.
select * from employees where employee_id in (100, 102, 105, 107);

-- 18. Wyświetl nazwisko, pensję i premię pracowników, których nazwisko zaczyna się na literę K.
select last_name as 'nazwisko', salary, commission_pct  from `employees` where last_name like 'K%';

-- 19. Wyświetl imiona i nazwiska pracowników, w których nazwisku występuje litera ‘i’, ‘a’ lub ‘o’.
-- select last_name as 'nazwisko', first_name as 'imie' from `employees` where last_name LIKE ('%i%', '%o%');
SELECT `FIRST_NAME`, `LAST_NAME` FROM `employees` WHERE `FIRST_NAME` LIKE ("%o%") OR `FIRST_NAME` LIKE ("%a%") OR `FIRST_NAME` LIKE ("%i%");


-- 20. Wyświetl imiona i nazwiska pracowników zatrudnionych w oddziale o identyfikatorze 60.
select last_name as 'nazwisko', first_name as 'imie' from `employees` e left join `departments` d on e.department_id=d.department_id where d.department_id=60;
select last_name as 'nazwisko', first_name as 'imie' from `employees`  where department_id=60;

-- 21. Wyświetl dane pracowników, którzy nie mają premii.
select * from `employees` where commission_pct is null;

-- 22. Wyświetl imiona i nazwiska pracowników, których druga litera imienia to ‘a’.
-- substr to funkcja ucinająca litery. W tym przypadku podajemy 3 parametry:
-- 		substr
-- 			last_name - zmienna na której działamy
-- 			2 		  - indeks litery (druga litera, indeksujemy od 1) od której zaczynamy
--			1 		  - ilość liter która nas interesuje. Chcemy żeby druga litera (tylko jedna litera) miała konkretną wartość
select last_name as 'nazwisko', first_name as 'imie' from `employees` where SUBSTR(first_name, 2, 1) = 'a';
SELECT `FIRST_NAME`, `LAST_NAME`  FROM `employees` WHERE `FIRST_NAME` LIKE "_a%";


-- 23. Wyświetl bez duplikatów identyfikatory oddziałów z tabeli employees.
select distinct department_id from `employees`;

-- 24. Wyświetl imiona, nazwiska i pensje pracowników, którzy zarabiają powyżej 10000.
select first_name as 'imie', last_name as 'nazwisko', salary from `employees` where salary > 10000;

-- 25. Wyświetl imiona, nazwiska i pensje powiększone o 20% pracowników zatrudnionych w oddziale o identyfikatorze 50.
select first_name as 'imie', last_name as 'nazwisko', salary*1.2 from `employees` where department_id=50;

-- 26. Wyświetl dane pracowników zatrudnionych w oddziale o identyfikatorze 100.
select * from `departments` where department_id > 100;

-- 27. Wyświetl dane oddziałów o identyfikatorze lokalizacji większym od 2000.
select * from `departments` where location_id > 2000;

-- 28. Wyświetl dane lokalizacji znajdujących się w miastach, których nazwy rozpoczynają się na literę ‘S’.
select * from `locations` where city like 'S%';

-- 29. Wyświetl bez duplikatów identyfikatory krajów z tabeli countries.
select distinct country_id from `countries`;

-- 30. Wyświetl nazwy krajów posortowane w porządku rosnącym.
select country_name from `countries` order by country_name asc;

-- 31. Wyświetl nazwiska, daty zatrudnienia pracowników, pensje i pensje po podwyżce ( powiększone o 1000) nadaj kolumnom aliasy.
select last_name as 'nazwisko', hire_date as 'data zatrudnienia', salary as 'pensja', salary+1000 as 'po podwyżce' from `employees` ;