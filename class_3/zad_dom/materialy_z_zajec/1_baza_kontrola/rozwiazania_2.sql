-- Podstawowe zapytania
-- 1) Wyświetlenie wszystkich strażników z tym, że zamiast kolumna 'imie' chciałbym żeby była kolumna imie_strażnika
select imie as 'Imie straznika' from straznik;

-- 2) Wyświetlić strażników którzy mają pensje (bez uwzględniania składki na ubezpieczenia) większe niż 1500zł
select * from straznik where (pensja + skladka_na_ubezpieczenie) > 1500;

-- 3) Wyświetlić strażników z pensją większą od 1500zł ale mniejszą niż 2500zł
select * from straznik where pensja between 1500 and 2500;

-- 4) Wyświetlić strażników ale bez strażników o nazwisku Nowak i Kowalczyk
select * from straznik where nazwisko not in ('Nowak', 'Kowalczyk');

-- 5) Wyświetlić strażników ale bez strażników o id 1,6,5 (z użyciem IN)
select * from straznik where id NOT IN (1, 5, 6);

-- 6) Wyświetlić strazników i pensje które są większe od 1500 ale po odjęciu "skladka_na_ubezpieczenie"
select * from straznik s where s.pensja-s.skladka_na_ubezpieczenie > 1500;

-- 7) Wyświetlić pasażerów posortowanych po nazwisku i imieniu
select * from pasazer order by nazwisko, imie;

-- 8) Wyświetlić strażników którzy mają nazwisko rozpoczynające się od "Kowal"
select * from straznik where nazwisko LIKE 'Kowal%';

-- 9) Wyświetlić strażników o nazwisku Nowak i którzy zostali zatrudnieni w roku 2017
--  select * from straznik where nazwisko= 'Nowak' and data_zatrudnienia >'2017-01-01';
select * from straznik where nazwisko= 'Nowak' and year(data_zatrudnienia) = 2017;

-- 10) Wyświetlić nazwisko+pensje strażników pomniejszone skladka_na_ubezpieczenie, kolumna ma się nazywać pensja_do_wyplaty
select nazwisko, (pensja-skladka_na_ubezpieczenie)  as pensja from straznik ;

-- 11) Wyświetlić wszystkich strażników aktualnych i archiwalnych (kontrola.straznik_archiwum) w jednej tabeli
select * from straznik UNION select * from straznik_archiwum;

-- 12) Wyświetlić strażnika który nie ma ustawionego pola skladka_na_ubezpieczenie (jest to NULL)
select * from straznik where skladka_na_ubezpieczenie IS NULL;

-- Używanie agregatów 
-- ---------------------------
-- 1) Napisać zapytania które poda sumę  pensji (pola pensja) dla wszystkich strażników
select sum(pensja) from straznik;

-- 2) Podać średnią pensję strażników 
select avg(pensja) from straznik;

-- 3) Wyświetlić największą pensje
select max(pensja) from straznik;

-- 4) Podac liczbę  pasażerów w systemie
select count(*) from pasazer;

-- 5) Podać liczbę strażników ale tych którzy mają uzupełnione pole skladka_na_ubezpieczenie
select count(*) from straznik where skladka_na_ubezpieczenie IS NOT NULL;

-- Zapytania z JOIN
-- -------------------
-- 1) Wyświetlić wszystkie kontrole przeprowadzone na  lotnisku Gdańsk
select * 
from kontrola k join numer_stanowiska n on k.id_numer_stanowiska = n.id 
where nazwa_portu='Gdańsk';

-- 2) Wyświetlić wszystkie kontrole przeprowadzone dla lotnisku Gdańsk przez strażnika/ów który ma nazwisko 'Nowak'
select * 
from straznik s join 
	(select * 
		from kontrola k join numer_stanowiska n on k.id_numer_stanowiska = n.id 
		where nazwa_portu='Gdańsk') k 
        on s.id = k.id_straznik
			where s.nazwisko='Nowak';

-- 3) Wyświetlić strażników i przeprowadzone przez nich kontrole jeśli strażnik nie ma kontroli to wyświetlamy informację 
-- o strażniku a w części kontroli wyświetlamy nulle 
select * 
from straznik s left join kontrola k
	on s.id = k.id_straznik;


-- 4) Wyświetlić wszystkie lotniska odwiedzone przez pasażera imie="Jan"  AND nazwisko="Brzechwa"
select * from numer_stanowiska s where exists 
															(select * 
															from kontrola k join pasazer p
																on k.id_pasazer = p.id 
																	where imie='Jan' AND nazwisko='Brzechwa' AND s.id = k.id_numer_stanowiska);

-- PODZAPYTANIA
-- 1) Wyświetlić wszystkie kontrole przeprowadzone dla lotniksa Gdańsk przez strażnika który ma największe 
-- zarobki
select * from kontrola where id_straznik IN 
		(select id from straznik where pensja = 
				(select max(pensja) from straznik));

-- select max(pensja) from straznik;
-- select id from straznik where pensja = 
-- 				(select max(pensja) from straznik);

-- 2) Wyświetlić z użyciem podzapytania wszystkiich pasażerów skontrolowanych przez strażnika o nazwisku "Nowak"
select * from pasazer p where EXISTS 
	( select * from kontrola k where EXISTS 
		(select * from straznik s where nazwisko = 'Nowak' AND s.id=k.id_straznik) AND p.id = k.id_pasazer);

-- 3) Wyświetlić strażników a w ostatniej kolumnie kwotę najwyższej pensji strażnika
select nazwisko,(select max(pensja) from straznik) from straznik;

-- 4) Wyświetlić strażników a w ostatniej kolumnie informację o ile mniej/więcej zarabia dany strażnik od średniej  
select nazwisko,((select avg(pensja) from straznik)-pensja) as 'Ile mniej od średniej' from straznik;


-- Zlozone
-- 26) Wyświetlić pasażera który  nigdy nie był kontrolowany. 

SELECT * FROM  pasazer p LEFT JOIN  kontrola k ON p.id=k.id_pasazer
 WHERE k.id_pasazer IS NULL;

-- 27) Znaleźć pasażera który odwiedził największą ilość lotnisk (użyć LIMIT), 
-- wyświetlić jaką liczbę lotnisk odwiedzili (jeśli pasażer odwiedził dwa razy to samo lotnisko
-- to zliczamy to jako jedno odwiedzenie)
SELECT id, imie,nazwisko, count(*) AS ilosc_lotnisk FROM (
  SELECT  DISTINCT p.id,p.imie, p.nazwisko, ns.nazwa_portu FROM  pasazer p 
  JOIN  kontrola k ON (k.id_pasazer=p.id) 
  JOIN  numer_stanowiska ns ON (k.id_numer_stanowiska=ns.id)
) T
GROUP BY id, imie,nazwisko
  ORDER BY  ilosc_lotnisk DESC LIMIT 1;

SELECT DISTINCT p.id,p.imie, p.nazwisko, ns.nazwa_portu FROM  pasazer p 
  JOIN  kontrola k ON (k.id_pasazer=p.id) 
  JOIN  numer_stanowiska ns ON (k.id_numer_stanowiska=ns.id);

SELECT * FROM kontrola JOIN straznik;

-- 28) Znaleźć 2 strażników którzy skontrolowali największą ilość pasażerów
-- (ponowna kontrola pasażera zliczana jest jako dodatkowa kontrola)

SELECT count(*) AS ilosc,id_straznik, imie, nazwisko  
FROM  kontrola  k JOIN  straznik s 
ON k.id_straznik=s.id
group BY id_straznik,imie, nazwisko
ORDER BY 1 DESC 
LIMIT 2;

-- 29) Znaleźć lotnisko na którym poleciała najmniejsza ilość pasażer
-- (ale większa od 0)

SELECT ns.nazwa_portu, count(*) AS liczba_kontroli FROM  kontrola k 
 JOIN  numer_stanowiska ns ON ns.id=k.id_numer_stanowiska 
   WHERE   wynik_kontroli=true
 GROUP BY ns.nazwa_portu ORDER BY liczba_kontroli ASC LIMIT 1;


-- 30) Znaleźć miesiac (w przeciagu całego okresu)  w którym był największy ruch na 
-- wszystkich lotniskach. Użyć	date_format()

SELECT DATE_FORMAT(czas_kontroli,'%Y-%m-01'),czas_kontroli FROM kontrola;

SELECT DATE_FORMAT(czas_kontroli,'%Y-%m-01'),count(*)  
FROM  kontrola GROUP BY DATE_FORMAT(czas_kontroli,'%Y-%m-01') 
ORDER BY 2 DESC LIMIT 1 ;

SELECT *,DATE_FORMAT(czas_kontroli,'%Y-%m-01')   FROM  kontrola ;

	
-- 31) Wyświetlić  ilość pasażerów w kolejnych latach dla każdego lotniska  
-- (lotnisko sortujemy według nazwy rosnąco a póxniej według roku)
--   Lotnisko ABC   2000   300
--   Lotnisko ABC   2001   400
--   Lotnisko BCD   2000   333
--   Lotnisko CDE   2000   323
--   Lotnisko CDE   2001   332

SELECT count(*), ns.nazwa_portu,DATE_FORMAT(k.czas_kontroli,'%Y-01-01') AS rok
  FROM  kontrola  k 
 JOIN  numer_stanowiska ns ON ns.id=k.id_numer_stanowiska
GROUP BY ns.nazwa_portu,DATE_FORMAT(k.czas_kontroli,'%Y-01-01')
ORDER BY ns.nazwa_portu,rok;

-- // to jesli bylaby mowa o unikalnych pasazerach
SELECT count(*),nazwa_portu,rok FROM (
SELECT DISTINCT ns.nazwa_portu,p.imie, p.nazwisko, DATE_FORMAT(k.czas_kontroli,'%Y-01-01') AS rok  FROM  kontrola  k JOIN  pasazer p ON k.id_straznik=p.id
 JOIN  numer_stanowiska ns ON ns.id=k.id_numer_stanowiska
) T  GROUP BY nazwa_portu,rok ORDER BY nazwa_portu,rok;
