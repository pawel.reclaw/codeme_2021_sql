-- Podstawowe zapytania
-- 1) Wyświetlenie wszystkich strażników z tym, że zamiast kolumna 'imie' chciałbym żeby była kolumna imie_strażnika
SELECT `id`, `imie` as 'imie_strażnika', `nazwisko`, `stopien`, `data_zatrudnienia`, `pensja`, `skladka_na_ubezpieczenie` FROM kontrola.straznik;
SELECT *, `imie` as 'imie_straznika' from `straznik`;
-- 
-- 2) Wyświetlić strażników którzy mają pensje (bez uwzględniania składni na ubezpieczenia) większe niż 1500zł
select * from straznik where (pensja + skladka_na_ubezpieczenie) > 1500;
-- 
-- 3) Wyświetlić strażników z pensją większą od 1500zł ale mniejszą niż 2500zł
select * from straznik where pensja between 1500 and 2500;
-- 
-- 4) Wyświetlić strażników ale bez strażników o nazwisku Nowak i Kowalczyk
select * from straznik where nazwisko not in ('Nowak', 'Kowalczyk');

-- 5) Wyświetlić strażników ale bez strażników o id 1,6,5 (z użyciem IN)
SELECT * FROM `straznik` WHERE `id` NOT IN (1, 5, 6);
SELECT * FROM `straznik` WHERE `id`<>1 and `id`<>5 and `id`<>6; -- niby działa, ale nieładnie jest tak napisać

-- 
-- 6) Wyświetlić strazników i pensje które są większe od 1500 ale po odjęciu "skladka_na_ubezpieczenie"
select * from straznik s where s.pensja-s.skladka_na_ubezpieczenie > 1500;
-- 
-- 7) Wyświetlić pasażerów posortowanych po nazwisku i imieniu
select * from pasazer order by nazwisko, imie;
-- 
-- 
-- 8) Wyświetlić strażników którzy mają nazwisko rozpoczynające się od "Kowal"
select * from straznik where nazwisko LIKE 'Kowal%';
-- 
-- 9) Wyświetlić strażników o nazwisku Nowak i którzy zostali zatrudnieni w tym roku (year - extract year from datetime)
select * from straznik where nazwisko= 'Nowak' and year(data_zatrudnienia) = 2020;
-- 
-- 
-- 10) Wyświetlić nazwisko+pensje strażników pomniejszone skladka_na_ubezpieczenie, kolumna ma się nazywać pensja_do_wyplaty
select nazwisko, (pensja-skladka_na_ubezpieczenie)  as 'pensja_do_wyplaty' from straznik ;
-- 11) Wyświetlić wszystkich strażników aktualnych i archiwalnych (kontrola.straznik_archiwum) w jednej tabeli 
select * from straznik UNION select * from straznik_archiwum;
-- 12) Wyświetlić strażnika który nie ma ustawionego pola skladka_na_ubezpieczenie (jest to NULL)
select * from straznik where skladka_na_ubezpieczenie IS NULL;
-- 
-- Używanie agregatów - Proszę 
-- ---------------------------
-- 1) Napisać zapytania które poda sumę  pensji (pola pensja) dla wszystkich strażników
select sum(pensja) from straznik;

-- 2) Podać średnią pensję strażników 
select avg(pensja) from straznik;

-- 3) Wyświetlić największą pensje
select max(pensja) from straznik;

-- 4) Podac liczbę  pasażerów w systemie
select count(*) from pasazer;

-- 5) Podać liczbę strażników ale tych którzy mają uzupełnione pole skladka_na_ubezpieczenie
select count(*) from straznik where skladka_na_ubezpieczenie IS NOT NULL;
-- 
-- Zapytania z JOIN
-- -------------------
-- 1) Wyświetlić wszystkie kontrole przeprowadzone na  lotnisku Gdańsk
SELECT * FROM `kontrola` k INNER JOIN `numer_stanowiska` ns ON k.`id_numer_stanowiska` = ns.`id` WHERE ns.`nazwa_portu`="Gdańsk";

-- Dziele to zapytanie na podzapytanie. 1 - szukam stanowisk z nazwą portu "Gdańsk
SELECT `id` FROM `numer_stanowiska` WHERE `nazwa_portu`='Gdańsk';
-- Zapytanie wykorzystujące powyższą linię jako podzapytanie ^^
SELECT * FROM `kontrola` k WHERE k.`id_numer_stanowiska` IN (SELECT `id` FROM `numer_stanowiska` WHERE `nazwa_portu`='Gdańsk');

-- 
-- 2) Wyświetlić wszystkie kontrole przeprowadzone dla lotnisku Gdańsk przez strażnika/ów który ma nazwisko 'Nowak'
SELECT * from `kontrola` k 
	INNER JOIN `straznik` s ON k.id_straznik = s.id 
		WHERE s.nazwisko = 'Nowak';

SELECT * FROM `kontrola` k 
	INNER JOIN `straznik` s ON k.id_straznik = s.id 
    INNER JOIN `numer_stanowiska` ns ON k.id_numer_stanowiska = ns.id
		WHERE s.`nazwisko` = 'Nowak' 
			AND ns.`nazwa_portu` = 'Gdańsk';

-- Podzieliliśmy zapytanie na podzapytania, podzapytanie o identyfikatory straznikow z danym nazwiskiem:
SELECT id from `straznik` WHERE nazwisko = 'Nowak';
SELECT * FROM `kontrola` k 
	WHERE k.`id_straznik` IN (SELECT `id` from `straznik` WHERE nazwisko = 'Nowak') -- 
		AND k.`id_numer_stanowiska` IN (SELECT `id` FROM `numer_stanowiska` WHERE `nazwa_portu`='Gdańsk'); -- 

-- 
-- 3) Wyświetlić strażników i przeprowadzone przez nich kontrole jeśli strażnik nie ma kontroli to wyświetlamy informację 
-- o strażniku a w części kontroli wyświetlamy nulle 
select * from straznik s left join kontrola k
	on s.id = k.id_straznik;


-- 4) Wyświetlić wszystkie lotniska odwiedzone przez pasażera imie="Jan"  AND nazwisko="Brzechwa"
-- select * 
-- 	from kontrola k join pasazer p
-- 		on k.id_pasazer = p.id 
-- 			where imie='Jan' AND nazwisko='Brzechwa' AND s.id = k.id_numer_stanowiska;
select distinct id_numer_stanowiska
	from kontrola k join pasazer p
		on k.id_pasazer = p.id where imie='Jan' AND nazwisko='Brzechwa';
        
select * from numer_stanowiska s
	where s.id IN (select distinct id_numer_stanowiska
						from kontrola k join pasazer p
							on k.id_pasazer = p.id where imie='Jan' AND nazwisko='Brzechwa');
        
select * from numer_stanowiska s where exists (select * 
															from kontrola k join pasazer p
																on k.id_pasazer = p.id 
																	where imie='Jan' AND nazwisko='Brzechwa' AND s.id = k.id_numer_stanowiska);
-- PODZAPYTANIA
-- 1) Wyświetlić wszystkie kontrole przeprowadzone dla lotniksa Gdańsk przez strażnika który ma największe zarobki
select * from kontrola where id_straznik IN 
		(select id from straznik where pensja = 
				(select max(pensja) from straznik));
-- 
-- 2) Wyświetlić z użyciem podzapytania wszystkiich pasażerów skontrolowanych przez strażnika o nazwisku "Nowak"
-- Wszystkie kontrole wykonane przez 'Nowaków'
(SELECT `id_pasazer` FROM `kontrola` k WHERE k.`id_numer_stanowiska` IN (SELECT `id` FROM `straznik` WHERE `nazwisko`='Nowak'));

-- Podstawową tabelą do której wykonujemy zapytanie, jest tabela `pasazer` ponieważ zawiera imiona i nazwiska pasazerow o które musimy odpytać. 
-- Do niej robię podstawowe zapytanie. (stąd początek zapytania to: "select * from PASAZER....")
SELECT DISTINCT `imie`, `nazwisko` FROM `pasazer` p
	INNER JOIN 
		(SELECT `id_pasazer` FROM `kontrola` k WHERE k.`id_numer_stanowiska` IN 
			(SELECT `id` FROM `straznik` s WHERE s.`nazwisko`='Nowak')) w
	ON p.`id` = w.`id_pasazer`;
    
SELECT * FROM `pasazer` p
	WHERE p.`id` IN 
		(SELECT `id_pasazer` FROM `kontrola` k WHERE k.`id_numer_stanowiska` IN 
			(SELECT `id` FROM `straznik` s WHERE s.`nazwisko`='Nowak'));

-- 
-- 3) Wyświetlić strażników a w ostatniej kolumnie kwotę najwyższej pensji strażnika
select nazwisko,(select max(pensja) from straznik) from straznik;

-- 4) Wyświetlić strażników a w ostatniej kolumnie informację o ile mniej/więcej zarabia dany strażnik od średniej  
select nazwisko,((select avg(pensja) from straznik)-pensja) as 'Ile mniej od średniej' from straznik;

-- 
-- Zlozone
-- 1) Wyświetlić pasażera który  nigdy nie był kontrolowany. 
SELECT * FROM  pasazer p LEFT JOIN  kontrola k ON p.id=k.id_pasazer WHERE k.id_pasazer IS NULL;
select * from pasazer p	where p.id not in (select id_pasazer from kontrola);


SELECT  DISTINCT p.id,p.imie, p.nazwisko, ns.nazwa_portu FROM  pasazer p 
  JOIN  kontrola k ON (k.id_pasazer=p.id) 
  JOIN  numer_stanowiska ns ON (k.id_numer_stanowiska=ns.id);
  
-- 2) Znaleźć pasażera który odwiedził największą ilość lotnisk (użyć LIMIT), wyświetlić jaką liczbę lotnisk odwiedzili.
SELECT id, imie,nazwisko, count(*) AS ilosc_lotnisk 
	FROM (SELECT  DISTINCT p.id,p.imie, p.nazwisko, ns.nazwa_portu FROM  pasazer p 
		  JOIN  kontrola k ON (k.id_pasazer=p.id) 
		  JOIN  numer_stanowiska ns ON (k.id_numer_stanowiska=ns.id) 
		 ) T
GROUP BY id, imie,nazwisko
  ORDER BY  ilosc_lotnisk DESC LIMIT 1;

-- 
-- 
select imie, nazwisko, count(*) from 
		(SELECT distinct imie, nazwisko, nazwa_portu FROM kontrola 
			JOIN numer_stanowiska on kontrola.id_numer_stanowiska = numer_stanowiska.id 
				join pasazer on kontrola.id_pasazer = pasazer.id) tabela
        group by imie, nazwisko
        order by count(*) desc limit 1;
        
select id_pasazer, count(*) from kontrola group by id_pasazer order by count(*) desc limit 1;

-- 
-- 3) Znaleźć 2 strażników którzy skontrolowali największą ilość klientów.
select imie,nazwisko,count(*) from kontrola k 
	inner join straznik s
		on k.id_straznik = s.id
	group by id_straznik
    order by count(*) desc
    limit 2;
    
-- 4) Znaleźć lotnisko przez które poleciała najmniejsza ilość pasażerów .
SELECT * FROM  kontrola k 
 JOIN  numer_stanowiska ns ON ns.id=k.id_numer_stanowiska 
   WHERE   wynik_kontroli=1;
 
SELECT ns.nazwa_portu, count(*) AS liczba_kontroli FROM  kontrola k 
 JOIN  numer_stanowiska ns ON ns.id=k.id_numer_stanowiska 
   WHERE   wynik_kontroli=1
 GROUP BY ns.nazwa_portu ORDER BY liczba_kontroli ASC LIMIT 1;
 
-- 5) Znaleźć miesiac (w przeciagu całego okresu)  w którym był największy ruch na wszystkich lotniskach / wybranym lotnisku. Użyć
-- 	date_trunc('month', timestamp '2001-02-16 20:38:40')
SELECT DATE_FORMAT(czas_kontroli,'%Y-%m'),czas_kontroli FROM kontrola;

SELECT count(*)  
	FROM  kontrola 	
		GROUP BY DATE_FORMAT(czas_kontroli,'%Y-%m') 
        ORDER BY count(*) DESC LIMIT 1 ;
        
SELECT DATE_FORMAT(czas_kontroli,'%Y-%m'),count(*)  
	FROM  kontrola 	
		GROUP BY DATE_FORMAT(czas_kontroli,'%Y-%m') 
        HAVING COUNT(*) = (SELECT count(*)  
							FROM  kontrola 	
								GROUP BY DATE_FORMAT(czas_kontroli,'%Y-%m') 
									ORDER BY count(*) DESC LIMIT 1);

                                    

-- ORDER BY count(*) desc limit 1;


-- 6) Wyświetlić  ilość pasażerów w kolejnych latach dla każdego lotniska  (lotnisko sortujemy według nazwy rosnąco a póxniej według roku)
--   Lotnisko ABC   2000   300
--   Lotnisko ABC   2001   400
--   Lotnisko BCD   2000   333
--   Lotnisko CDE   2000   323
--   Lotnisko CDE   2001   332
SELECT  ns.nazwa_portu, YEAR(k.czas_kontroli) AS rok,count(*) as 'Ilość pasażerów' 
	FROM  kontrola  k 
		JOIN  numer_stanowiska ns ON ns.id=k.id_numer_stanowiska
	GROUP BY ns.nazwa_portu, YEAR(k.czas_kontroli)
	ORDER BY ns.nazwa_portu,rok;


SELECT DISTINCT ns.nazwa_portu,p.imie, p.nazwisko, YEAR(k.czas_kontroli) AS rok  
		FROM  kontrola  k 
			JOIN  pasazer p 
				ON k.id_straznik=p.id 
			JOIN  numer_stanowiska ns 
				ON ns.id=k.id_numer_stanowiska;
                
-- szukanie unikalnych pasażerów (jeśli Jan Brzechwa przelatywał przez lotnisk dwukrotnie w danym roku, to jest liczony jeden raz)
SELECT nazwa_portu,rok,count(*) AS 'Ilosc pasazerów'
	FROM  (SELECT DISTINCT ns.nazwa_portu,p.imie, p.nazwisko, YEAR(k.czas_kontroli) AS rok  
				FROM  kontrola  k 
					JOIN  pasazer p 
						ON k.id_straznik=p.id 
					JOIN  numer_stanowiska ns 
						ON ns.id=k.id_numer_stanowiska) T  
	GROUP BY nazwa_portu,rok ORDER BY nazwa_portu,rok;

-- MODYFIKACJA DANYCH - NIE ROBIMY, CHYBA ŻE NAM SIĘ NUDZI
-- 1) Umieścić wiersz z swoimi danymi w tablicy pasażera i dodać kontrole na lotnisku Gdańsk przez strażnika id=1 w dniu dzisiejszym
-- 2) Zmienić nazwisko strażników z 'Nowak' na 'Nowakowski' przy okazji zwiększając im pensje o 10%
-- 3) Skasować wiersz  z swoim wpisem w tablicy pasażer.
-- 4) Skasoważ strażnika który skontrolował największa liczbę pasażerów.